# Hometask 2  

## Task 1

```python
def count_negatives(number: list) -> int:
    return len(tuple(filter(lambda x: x < 0, number)))
```

## Task 2

```python
def change_position(players: list):
    first_place = players.pop(0)
    last_place = players.pop(len(players) - 1)
    players.insert(0, last_place)
    players.append(first_place)
```

## Task 3

```python
def swap(string: str):
    string = string.split(" ")
    first_word = string.pop(1)
    second_word = string.pop(8)
    string.insert(1, second_word)
    string.insert(9, first_word)
```
