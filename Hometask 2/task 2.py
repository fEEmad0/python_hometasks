def change_position(players: list):
    first_place = players.pop(0)
    last_place = players.pop(len(players) - 1)
    players.insert(0, last_place)
    players.append(first_place)
    print(*players)


if __name__ == '__main__':
    change_position(
        ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']
    )
