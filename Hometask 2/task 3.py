def swap(string: str):
    string = string.split(" ")
    first_word = string.pop(1)
    second_word = string.pop(8)
    string.insert(1, second_word)
    string.insert(9, first_word)
    print(*string)


if __name__ == '__main__':
    swap("The reasonable man adapts himself to the world;"
         " the unreasonable one persists in trying to adapt the world to himself")
