# Hometask 1
First task: "PEP 8 - the Style Guide for Python Code" has been read.

Second task: 
```python
# Use Heron's formula to find the area
from math import sqrt

a, b, c = 4.5, 5.9, 9
p = (a + b + c) / 2
S = sqrt(p * (p - a) * (p - b) * (p - c))
print("%.2f" % S)
```