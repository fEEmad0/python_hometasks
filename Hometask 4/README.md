# Hometask 4  

## Task 1

```python
def transpose(array):
    new_array = []
    for i in range(len(array[0])):
        tmp = []
        for j in range(len(array)):
            tmp.append(array[j][i])
        new_array.append(tmp)
    return new_array
```

## Task 2

```python
def factorial(x: int) -> int:
    if x == 1:
        return 1
    else:
        return x * factorial(x - 1)
```

## Task 3

```python
def fib_seq(x: int) -> int:
    a, b, sum = 0, 1, 1
    for i in range(1, x - 1):
        a, b = b, a + b
        sum = sum + b
        print(f'{b} ', end=' ')
    print()
    return sum
```

## Task 4

```python
def sum_of_1(x: int) -> int:
    res = 0
    while x != 0:
        res += 1 if (x & 1 == 1) else 0
        x = x >> 1
    return res
```
