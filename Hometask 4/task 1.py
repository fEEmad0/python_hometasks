def transpose(array):
    new_array = []
    for i in range(len(array[0])):
        tmp = []
        for j in range(len(array)):
            tmp.append(array[j][i])
        new_array.append(tmp)
    return new_array


if __name__ == '__main__':
    print(transpose([[0, 1, 9], [9, 8, 5], [7, 3, 1], [3, 4, 0]]))
