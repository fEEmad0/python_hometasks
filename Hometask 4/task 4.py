def sum_of_1(x: int) -> int:
    res = 0
    while x != 0:
        res += 1 if (x & 1 == 1) else 0
        x = x >> 1
    return res


if __name__ == '__main__':
    print(sum_of_1(int(input("Введите число "))))
