def fib_seq(x: int) -> int:
    a, b, sum = 0, 1, 1
    for i in range(1, x - 1):
        a, b = b, a + b
        sum = sum + b
        print(f'{b} ', end=' ')
    print()
    return sum


if __name__ == '__main__':
    print(fib_seq(int(input("Введите число "))))
