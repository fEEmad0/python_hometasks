# Hometask 3  

## Task 1

```python
def validate_point(x: float, y: float) -> bool:
    if -1 <= x <= 1:
        if 0 <= y <= 1:
            if y >= abs(x):
                return True
    return False
```

## Task 2

```python
def check_div(x: int) -> str:
    if 1 > x or x > 100:
        return f'{x} is out of range [1, 100]'
    if x % 3 == 0 and x % 5 == 0:
        return 'FizzBuzz'
    elif x % 3 == 0:
        return 'Fizz'
    elif x % 5 == 0:
        return 'Buzz'
    else:
        return f'{x}'
```

## Task 3

```python
def play(first_card: str, second_card: str) -> str:
    if not (first_card in CARDS.keys() and second_card in CARDS.keys()):
        return 'Shame on you! Do not cheat!'
    sum_points = CARDS.get(first_card) + CARDS.get(second_card)
    if sum_points >= 10:
        sum_points %= 10
    return str(sum_points)
```
