def validate_point(x: float, y: float) -> bool:
    if -1 <= x <= 1:
        if 0 <= y <= 1:
            if y >= abs(x):
                return True
    return False


if __name__ == '__main__':
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))

    print(f'Is point in shadow area: {validate_point(x, y)}')
