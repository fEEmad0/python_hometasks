def check_div(x: int) -> str:
    if 1 > x or x > 100:
        return f'{x} is out of range [1, 100]'
    if x % 3 == 0 and x % 5 == 0:
        return 'FizzBuzz'
    elif x % 3 == 0:
        return 'Fizz'
    elif x % 5 == 0:
        return 'Buzz'
    else:
        return f'{x}'


if __name__ == '__main__':
    for i in range(0, 10):
        print(check_div(int(input('Enter the number: '))))
