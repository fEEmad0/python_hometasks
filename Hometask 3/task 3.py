CARDS = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8,
         '9': 9, '10': 0, 'J': 0, 'Q': 0, 'K': 0, 'A': 1}


def play(first_card: str, second_card: str) -> str:
    if not (first_card in CARDS.keys() and second_card in CARDS.keys()):
        return 'Shame on you! Do not cheat!'
    sum_points = CARDS.get(first_card) + CARDS.get(second_card)
    if sum_points >= 10:
        sum_points %= 10
    return str(sum_points)


if __name__ == '__main__':
    first_card = str(input('Play first card: '))
    second_card = str(input('Play second card: '))

    print('Your result: ', play(first_card, second_card))
