class Node:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next

    def __iter__(self):
        return self

    # def __next__(self):
    #     if self.next is None:
    #         raise StopIteration
    #     else:
    #         yield self.next


class CustomList:
    def __init__(self, value=None):
        self.head = None

        if value:
            for val in value:
                self.append(val)

    # Add value to the end of list
    def append(self, value):
        if self.head is None:
            self.head = Node(value)
            return

        current = self.head
        # if current.next is not None:
        #     print(current.next.data)
        print()
        while current.next is not None:
            # print(current, " ", current.data)
            current = current.next
        current.next = Node(value)

    # Insert value at the selected position
    def insert(self, index, value):
        if 0 > index | index > len(self):
            raise ValueError
        elif index == 0:
            self.head = Node(value, self.head)
            return
        elif index == len(self):
            self(-1).next = Node(value)
            return
        self[index] = value

    def clear(self):
        self.head = None

    def remove(self, value):
        if not self.head:
            raise ValueError('List does not have current value')
        elif self.head.data == value:
            self.head = self.head.next
            return

        prev_node = self.head
        for node in self:
            if node.data == value:
                prev_node.next = node.next
                return
            prev_node = node

        raise ValueError('List does not have current value')

    def __len__(self):
        if self.head is None:
            return 0

        length = 0
        for _ in self:
            length += 1
            return length

    def __repr__(self):
        if self.head is None:
            return None

        node = self.head
        nodes = []
        while node is not None:
            nodes.append(node.data)
            node = node.next
            # print(node.data)
        return "[" + "->".join([repr(item) for item in nodes]) + "]"

    def __getitem__(self, key):
        if isinstance(key, slice):
            start, stop, step = key.indices(len(self))
            return CustomList([self[i] for i in range(start, stop, step)])
        elif isinstance(key, int):
            if key >= len(self):
                raise IndexError("Current index is out of range")
            elif key < 0:
                key = len(self) + key
            for i, item in enumerate(self):
                print(item.data)
                print(key)
                if i == key:
                    return item.data
            raise IndexError("Current index is out of range")

    def __set__(self, index, value):
        if index >= len(self):
            raise IndexError("Current index is out of range")

        for i, item in enumerate(self):
            if i == index:
                item.data = value
                return
        raise IndexError("Current index is out of range")

    def __delitem__(self, index):
        self.remove(self[index])

    def __iter__(self):
        self.current = self.head
        return self

    def __next__(self):
        if self.current is None:
            raise StopIteration
        else:
            yield self.current
            self.current = self.current.next

    def __str__(self):
        return f"{self.head.data}"


if __name__ == '__main__':
    node = CustomList()
    node.append(4)
    # print(node)
    node.append(8)
    # print(len(node))
    node.append(5)
    node.append(9)
    # print(len(node))

    # for _ in node:
    #     print(node)

    it = iter(node)
    print(node[0])
    print(next(it))
    print(node[0])
    print(next(it))
    print(node[0])
    print(next(it))
    print(node[0])
    print(next(it))
    print(node)
    print(next(it))
    print(node)
    print(next(it))
