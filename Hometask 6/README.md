# Hometask 6

## Task 1

```python
def combine_dicts(*dic: dict) -> dict:
    new_dic = {}
    for i in range(len(dic)):
        for key in dic[i]:
            if key in new_dic:
                new_dic[key] += dic[i][key]
            else:
                new_dic[key] = dic[i][key]
                pass
    return new_dic
```

## Task 2

Not finished yet.
```python
```
