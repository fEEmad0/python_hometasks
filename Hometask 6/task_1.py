def combine_dicts(*dic: dict) -> dict:
    new_dic = {}
    for i in range(len(dic)):
        for key in dic[i]:
            if key in new_dic:
                new_dic[key] += dic[i][key]
            else:
                new_dic[key] = dic[i][key]
                pass
    return new_dic


if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}
    print(combine_dicts(dict_1, dict_2, dict_3))
