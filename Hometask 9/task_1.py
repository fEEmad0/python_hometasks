def replace_quote(string: str) -> str:
    single = "'"
    double = '"'
    swap = "swap"
    string = string.replace(single, swap)
    string = string.replace(double, single)
    string = string.replace(swap, double)
    return string


if __name__ == '__main__':
    string = "Hello \' World \" with \' some \" text \""
    print(string)
    print(replace_quote(string))
