import re


def get_shortest_word(string: str) -> str:
    result = re.finditer(r'[\w]+[\b]', string)
    cont = [x.group() for x in result]
    return min(cont, key=len)


if __name__ == '__main__':
    text_1 = "Some set of long and short word"
    text_2 = "Another sample of word with spaces"

    print(text_1)
    print(text_2)
