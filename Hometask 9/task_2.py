def is_palindrome(string: str) -> bool:
    string = preprocessing(string)
    for i in range(len(string) // 2):
        if string[i] != string[len(string) - i - 1]:
            return False
    return True


def preprocessing(string: str) -> str:
    string = string.lower()
    red_symb = [".", ",", ";", ":", "!", "?", "-", " ", "\'", "\""]
    for el in red_symb:
        string = string.replace(el, "")
    return string


if __name__ == '__main__':
    print(is_palindrome("level"))
