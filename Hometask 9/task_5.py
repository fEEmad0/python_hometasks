def count_letters(line: str) -> dict:
    result = {}
    for char in line:
        if char not in result:
            result[char] = 1
        else:
            result[char] += 1
    return result


if __name__ == '__main__':
    print(count_letters("For example we have this sentence"))
