import task_1 as t


def transform(ls: list, order: t.SortOrder) -> list:
    if not t.is_sorted(ls, order):
        return ls
    for i in range(len(ls)):
        ls[i] = ls[i] + i
    return ls


if __name__ == '__main__':
    print(transform([15, 10, 3], t.SortOrder.DESCENDING))
