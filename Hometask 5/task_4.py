def SumGeometricElements(a_1: int, t: float, alim: int) -> float:
    if not 0 < t < 1:
        return -1
    res = 0
    while a_1 > alim:
        res += a_1
        a_1 *= t
    return res


if __name__ == '__main__':
    print(SumGeometricElements(100, 0.5, 10))
