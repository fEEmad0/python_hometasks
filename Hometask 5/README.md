# Hometask 5  

## Task 1

```python
def is_sorted(ls: list, order: SortOrder) -> bool:
    flag = True
    if order.value.__eq__(SortOrder.ASCENDING.value):
        def comp(x_1, x_2): return True if x_1 < x_2 else False
    elif order.value.__eq__(SortOrder.DESCENDING.value):
        def comp(x_1, x_2): return True if x_1 > x_2 else False
    else:
        return False
    for i in range(len(ls) - 1):
        flag &= comp(ls[i], ls[i + 1])
    return flag
```

## Task 2

```python
import task_1 as t


def transform(ls: list, order: t.SortOrder) -> list:
    if not t.is_sorted(ls, order):
        return ls
    for i in range(len(ls)):
        ls[i] = ls[i] + i
    return ls
```

## Task 3

```python
def MultArithmeticElements(a_1: int, t: int, n: int) -> int:
    res = 1
    for i in range(n):
        res *= a_1 + t * i
    return res
```

## Task 4

```python
def SumGeometricElements(a_1: int, t: float, alim: int) -> float:
    if not 0 < t < 1:
        return -1
    res = 0
    while a_1 > alim:
        res += a_1
        a_1 *= t
    return res
```
