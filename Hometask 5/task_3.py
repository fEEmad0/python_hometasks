def MultArithmeticElements(a_1: int, t: int, n: int) -> int:
    res = 1
    for i in range(n):
        res *= a_1 + t * i
    return res


if __name__ == '__main__':
    print(MultArithmeticElements(5, 3, 4))
