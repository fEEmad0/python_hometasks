from enum import Enum


class SortOrder(Enum):
    ASCENDING = 'ascending'
    DESCENDING = 'descending'


def is_sorted(ls: list, order: SortOrder) -> bool:
    flag = True
    if order.value.__eq__(SortOrder.ASCENDING.value):
        def comp(x_1, x_2): return True if x_1 < x_2 else False
    elif order.value.__eq__(SortOrder.DESCENDING.value):
        def comp(x_1, x_2): return True if x_1 > x_2 else False
    else:
        return False
    for i in range(len(ls) - 1):
        flag &= comp(ls[i], ls[i + 1])
    return flag


if __name__ == '__main__':
    print(is_sorted([5, 4, 2, 1], SortOrder.DESCENDING))
