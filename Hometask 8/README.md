# Hometask 8

## Task 1

Entering a number less than or equal to 7 will result in an assert error message with the text 'Not enough'.

## Task 2

An error will be caused when a function is called because the bar function is not defined. At the same time, only the message in the block finally will be output, because it is mandatory.

## Task 3

Must return value 2, as the block finally is more important

## Task 4

Since there will be no error in line output, the program output will be only values 1 and 2.

## Task 5

Since '1' != 1, the raise "Error" line should be executed, but this error is not inherited from the BaseException class, which will cause another error

## Task 6

The program will always ask us to enter the name of the file displaying the message we specified if it is impossible to find it.
